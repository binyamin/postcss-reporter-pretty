# postcss-reporter-pretty

A pretty PostCSS reporter for [Deno](https://deno.land/). Modeled after
[vfile-reporter-pretty](https://github.com/vfile/vfile-reporter-pretty/) and
[eslint-formatter-pretty](https://github.com/sindresorhus/eslint-formatter-pretty/).

## Usage

The module's default export is a PostCSS plugin.

## Contributing

All input is welcome; feel free to
[open an issue](https://gitlab.com/binyamin/postcss-reporter-pretty/-/issues/new).
Please remember to be a
[mensch](https://www.merriam-webster.com/dictionary/mensch). If you want to
program, you can browse
[the issue list](https://gitlab.com/binyamin/postcss-reporter-pretty/-/issues).

## Legal

All source-code is provided under the terms of
[the MIT license](https://gitlab.com/binyamin/postcss-reporter-pretty/-/blob/main/LICENSE).
Copyright 2023 Binyamin Aron Green.
