# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project tries to adhere to
[Semantic Versioning (SemVer)](https://semver.org/spec/v2.0.0.html).

<!--
	**Added** for new features.
	**Changed** for changes in existing functionality.
	**Deprecated** for soon-to-be removed features.
	**Removed** for now removed features.
	**Fixed** for any bug fixes.
	**Security** in case of vulnerabilities.
-->

## [0.1.1] -2023-08-22

### Fixed

- Update dependencies

## [0.1.0] - 2023-04-11

### Added

- Initial Release

[0.1.0]: https://gitlab.com/binyamin/postcss-reporter-pretty/-/commits/v0.1.0
[0.1.1]: https://gitlab.com/binyamin/postcss-reporter-pretty/-/commits/v0.1.1
