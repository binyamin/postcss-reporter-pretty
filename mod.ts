/**
 * A pretty PostCSS reporter for [Deno](https://deno.land/).
 * Modeled after [vfile-reporter-pretty](https://github.com/vfile/vfile-reporter-pretty/) and [eslint-formatter-pretty](https://github.com/sindresorhus/eslint-formatter-pretty/).
 * @module
 */
export { default, type Options } from './src/reporter.ts';
