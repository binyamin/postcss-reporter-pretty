import { postcss } from './deps.ts';
import { createFormatter } from './formatter.ts';
import { getLocation } from './util.ts';

export type Options = {
	/** @default ['warning'] */
	messageTypes?: string[];
};

const reporter: postcss.PluginCreator<Options> = function (options) {
	options ??= {};
	options.messageTypes ??= ['warning'];

	const format = createFormatter();

	return {
		postcssPlugin: 'postcss-reporter-pretty',
		OnceExit(_root, { result }) {
			const messages = result.messages.filter((v) => {
				return options!.messageTypes!.includes(v.type);
			});

			if (messages.length === 0) return;

			const sourceName = result.root.source?.input.file ||
				result.root.source?.input.id || '<raw>';

			const groupedMessages = messages.reduce((groups, msg) => {
				const key = getLocation(msg).file || sourceName;

				groups[key] ??= [];
				groups[key].push(msg);

				return groups;
			}, {} as Record<string, postcss.Message[]>);

			console.error(format(groupedMessages));
		},
	};
};

reporter.postcss = true;

export default reporter;
