import { postcss } from './deps.ts';

export function getLocation(message: postcss.Warning | postcss.Message) {
	const location: {
		line: number;
		column: number;
		file?: string;
	} = {
		line: message.line,
		column: message.column,
	};

	const messageInput = message.node?.source?.input as postcss.Input | undefined;

	if (!messageInput) return location;

	const originLocation = messageInput?.origin(message.line, message.column);

	if (originLocation) return originLocation;

	location.file = messageInput.file || messageInput.id;
	return location;
}
