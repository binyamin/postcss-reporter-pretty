export { default as postcss } from 'npm:postcss@^8.4.21';
export * as path from 'https://deno.land/std@0.195.0/path/mod.ts';
export * as colors from 'https://deno.land/std@0.195.0/fmt/colors.ts';
export { sortBy } from 'https://deno.land/std@0.195.0/collections/sort_by.ts';
