import { colors, path, postcss, sortBy } from './deps.ts';

export type FormatOptions = {
	/** @default 'position' */
	sort?: 'position' | 'natural';
	/** @default false */
	reversed?: boolean;
	/** @default true */
	showLineNumbers?: boolean;
};

const symbols = {
	info: colors.blue('ℹ'),
	warning: colors.yellow('⚠'),
	error: colors.red('✖'),
	success: colors.green('✔'),
};

function sorter(options: Required<FormatOptions>) {
	if (options.sort === 'position') {
		return (messages: (postcss.Message | postcss.Warning)[]) => {
			const array = sortBy(
				sortBy(messages, (m) => {
					return m.line || -1;
				}),
				(m) => {
					return m.column || -1;
				},
			);

			if (options.reversed) return array.toReversed();
			return array;
		};
	}

	return (messages: (postcss.Message | postcss.Warning)[]) => {
		if (options.reversed) return messages.toReversed();
		return messages;
	};
}

export function createFormatter(options?: FormatOptions) {
	options ??= {};
	options.sort ??= 'position';
	options.reversed ??= false;
	options.showLineNumbers ??= true;

	const sort = sorter(options as Required<FormatOptions>);

	return (groups: Record<string, postcss.Message[]>) => {
		const lines: string[] = [];

		let maxLineWidth = 0;
		let maxColumnWidth = 0;
		let maxMessageWidth = 0;

		let warningCount = 0;

		for (const entry of Object.entries(groups)) {
			const source = entry[0];
			const messages = sort(entry[1]);
			if (messages.length === 0) continue;

			if (lines.length > 0) {
				lines.push('\n');
			}

			// Header
			// Add the line number so it's Command-click'able in some terminals
			// Use dim & gray for terminals like iTerm that don't support `hidden`
			const m = messages[0];
			let position = '';
			if (options!.showLineNumbers || m.line || m.column) {
				position = colors.hidden(
					colors.dim(colors.gray(`:${m.line}:${m.column}`)),
				);
			}

			lines.push(
				'  ' + colors.underline(path.relative('.', source)) + position,
			);

			for (const m of messages) {
				if (m.type === 'warning') warningCount++;

				const ln = String(m.line || 0);
				const col = String(m.column || 0);

				maxLineWidth = Math.max(ln.length, maxLineWidth);
				maxColumnWidth = Math.max(col.length, maxColumnWidth);
				maxMessageWidth = Math.max(m.text.length, maxMessageWidth);

				const line = [''];

				line.push(m.type === 'warning' ? symbols.warning : symbols.info);

				if (options!.showLineNumbers || m.line || m.column) {
					line.push(
						' '.repeat(maxLineWidth - ln.length) +
							colors.dim(m.line + colors.gray(':') + m.column),
					);
				}

				line.push(' '.repeat(maxColumnWidth - col.length) + m.text);

				if (m.plugin) {
					line.push(
						' '.repeat(maxMessageWidth - m.text.length) +
							colors.dim(m.plugin),
					);
				}

				lines.push(line.join('  '));
			}
		}

		let output = lines.join('\n') + '\n\n';

		if (warningCount > 0) {
			output += '  ' +
				colors.yellow(
					warningCount +
						' warning' +
						(warningCount > 1 ? 's' : ''),
				) + '\n';
		}

		return warningCount > 0 ? output : '';
	};
}
